const solve = require('./main');

test('run 1 000 elements',()=>{
    expect(solve(1000)).toBeTruthy();
});

test('run 100 000 elements',()=>{
    expect(solve(100000)).toBe(true)
});

test('run 10 000 000 elements',()=>{
    expect(solve(10000000)).toBe(true)
});

